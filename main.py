
# Tic-Tac-Toe Program using 
# random number in Python 
  
# importing all necessary libraries 
import numpy as np 
import random 
from time import sleep 
  
# Creates an empty board 
def create_board(): 
    return(np.array([[0, 0, 0], 
                     [0, 0, 0], 
                     [0, 0, 0]])) 
  
# Check for empty places on board 
def possibilities(board): 
    l = [] 
      
    for i in range(len(board)): 
        for j in range(len(board)): 
              
            if board[i][j] == 0: 
                l.append((i, j)) 
    return(l) 
  
# Select a random place for the player 
def random_place(board, player): 
    selection = possibilities(board) 
    current_loc = random.choice(selection) 
    board[current_loc] = player
    print("Random Placement")
    return(board) 

# Win if possible, else block a win if possible, else random
def win_block(board, player):
    if win_now(board, player):
        return(board)
    if block_now(board, player):
        return(board)

    return random_place(board, player)

# Win if possible.
#
# Returns True if won, else False.
def win_now(board, player):
    winning_selection = winning_moves(board, player)
    if len(winning_selection) > 0:
        current_loc = random.choice(winning_selection)
        board[current_loc] = player
        print("Winning")
        return True
    else:
        return False

# Block if possible.
#
# Returns True if blocked, else False.
def block_now(board, player):
    blocking_selection = winning_moves(board, opposing_player(player))
    if len(blocking_selection) > 0:
        current_loc = random.choice(blocking_selection)
        board[current_loc] = player
        print("Blocking")
        return True
    else:
        return False

# Returns all places on the board that would complete a line for the given player
def winning_moves(board, player):
    winning_points = []
    for line in three_element_lines():
        point = winning_point(board, line, player)
        if point is not None:
            winning_points.append(point)
    return(winning_points)

# Returns an array of lines (which is an array of points)
#
# These lines are the only possible groups of winning points.
def three_element_lines():
    return([
        [(0,0), (0,1), (0,2)],
        [(1,0), (1,1), (1,2)],
        [(2,0), (2,1), (2,2)],
        [(0,0), (1,0), (2,0)],
        [(0,1), (1,1), (2,1)],
        [(0,2), (1,2), (2,2)],
        [(0,0), (1,1), (2,2)],
        [(0,2), (1,1), (2,0)],
    ])

# Returns a point in the line if it would make a winning line for the given player.
#
# Else returns None.
def winning_point(board, line, player):
    a = board[line[0]]
    b = board[line[1]]
    c = board[line[2]]
    point = None
    if ((a != player and a != 0) or (b != player and b != 0) or (c != player and c != 0)):
        point = None
    elif (a == player):
        if (b == player):
            point = line[2]
        elif (c == player):
            point = line[1]
    elif (b == player and c == player):
        point = line[0]
    return(point)
  
# Returns the opponent's int
def opposing_player(player):
    if player == 1:
        return 2
    return 1

# Returns a winning player, else None
def find_winner(board):
    winner = None
    for line in three_element_lines():
        if all_points_contain_same_player(board, line):
            winner = board[line[0]]
    return(winner)

# Returns True if all points in the given line are owned by a single player
def all_points_contain_same_player(board, line):
    player = 0
    for point in line:
        point_player = board[point]
        if point_player == 0:        # this line has open spots
            return(False)
        if player == 0:              # first element
            player = point_player
        elif player != point_player: # different players in the line
            return(False)

    # if we finish the loop without returning early, then one player owns the line
    return(True)

# Returns True if board is full or won
def game_over(board):
    return(np.all(board != 0) or find_winner(board) is not None)
  
# Main function to start the game 
def play_game(): 
    board, counter = create_board(), 1
    print(board) 
    sleep(0.1)
      
    while not game_over(board): 
        for player in [1, 2]: 
            board = win_block(board, player) 
            print("Board after " + str(counter) + " move") 
            print(board) 
            sleep(0.1) 
            counter += 1
            if game_over(board):
                break
    return(find_winner(board)) 
  
# Driver Code 
print("Winner is: " + str(play_game())) 
